#! /usr/bin/env sh

# READ ALL ENV VARS
while IFS='=' read -r -d '' n v; do
    # SANITIZE VALUES remplace / for \/ 
    # EXAMPLE https://example.com TO https:\/\/example.com INCLUDE SCAPE FOR WORK ON SED
    # echo $v
    a=${v//\//\\/}
    # echo $a
    sed -i 's/'^[[:space:]]*$n[[:graph:]].*$'/'"${n}"': "'"${a}"'",/g' /var/www/app/config/front.env.js
    # printf "'%s'='%s'\n" "$n" "$v"
done < <(env -0)

nginx -g 'daemon off;'