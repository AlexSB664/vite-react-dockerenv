### Stage 1 BUILD APP
FROM node:lts-bullseye as build
WORKDIR /app
COPY ./frontend/package*.json ./
RUN npm ci
COPY frontend .
RUN npm run build

### Stage 2 Serve Files
FROM nginx:alpine
ADD ./frontend/config/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist /var/www/app/
COPY ./frontend/config/front.env.example.js /var/www/app/config/front.env.js
COPY ./frontend/config/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
EXPOSE 80
# CMD ["nginx","-g","daemon off;"]
ENTRYPOINT [ "/entrypoint.sh" ]